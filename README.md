<h1 style="text-align: center;">Prototype App</h1>

## Theory

For a description of the Prototype design pattern, you can refer
to the: [Prototype](docs/prototype.md) summary.

## Usage

### Prerequisites

Executing these commands requires an installed version
of [Docker Compose](https://docs.docker.com/compose/install/).

### Running the application

    docker compose run --rm prototype_app

<div align="center">
<img src="docs/prototype_demo.gif" alt="drawing"/>
</div>

### Running the tests

    docker compose run --rm prototype_app poetry run tests

## Development setup

The included project setting files can be used to continue development in PyCharm.

For a detailed description refer to [PyCharm project setup](docs/pycharm_project_setup.md).


