<div align="center">
  <h1>Prototype</h1>
</div>

<div align="center">
  <img src="prototype_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Prototype is a creational pattern for copying existing objects without becoming dependent on
their classes.**

### Real-World Analogy

_Cell division._

During cell division (clone), a pair of identical cells is formed.
The original cell (prototype) provides the organelles, molecules, and structures to the new cell.

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

• Prototype (Graphic)- declares an interface for cloning itself. • ConcretePrototype (Staff, WholeNote, HalfNote)-
implements an operation for cloning itself. • Client (GraphicTool)- creates a new object by asking a prototype to clone
itself.

### Collaborations

...

A client asks a prototype to clone itself

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

Use the Prototype pattern when a system should be independent of how its products are created, composed, and
represented; and
• when the classes to instantiate are specified at run-time, for example, by dynamic loading; or
• to avoid building a class hierarchy of factories that parallels the class hierarchy of products; or
• when instances of a class can have one of only a few different combinations of state. It may be more convenient to
install a corresponding
number of prototypes and clone them rather than instantiating the class manually, each time with the appropriate state.

### Motivation

- ...

You could build an editor for music scores by customizing a general framework for graphical editors and adding new
objects that represent notes, rests, and staves Users will click on the quarter-note tool and use it to add quarter
notes to the score. Or they can use the move tool to move a note up or down on the staff, thereby changing its pitch.
But GraphicTool presents a problem to the framework designer. The classes for notes and staves are specific to our
application, but the GraphicTool class belongs to the framework.
how can the framework use it to parameterize instances of GraphicTool by the class of Graphic they're supposed to
create?

---
You could build an editor for music scores by customizing a general framework
for graphical editors and adding new objects that represent notes, rests, and
staves. The editor framework may have a palette of tools for adding these music
objects to the score. The palette would also include tools for selecting, moving,
and otherwise manipulating music objects. Users will click on the quarter-note
tool and use it to add quarter notes to the score. Or they can use the move tool to
move a note up or down on the staff, thereby changing its pitch.

Let's assume the framework provides an abstract Graphic class for graphical components, like notes and staves.
Moreover, it'll provide an abstract Tool class for
defining tools like those in the palette. The framework also predefines a GraphicTool subclass for tools that create
instances of graphical objects and add them to
the document.

But GraphicToolpresents a problem to the framework designer. The classes for
notes and staves are specific to our application, but the GraphicTool class belongs
to the framework. GraphicTooldoesn't know how to create instances of our music
classes to add to the score. We could subclass GraphicToolfor each kind of music
object, but that would produce lots of subclasses that differ only in the kind of
music object they instantiate.We know object composition is a flexible alternative
to subclassing. The question is, how can the framework use it to parameterize
instances of GraphicTool by the class of Graphic they're supposed to create?

The solution lies in making GraphicTool create a new Graphic by copying or
"cloning" an instance of a Graphic subclass. We call this instance a prototype.
GraphicTool is parameterized by the prototype it should clone and add to the
document. If all Graphic subclasses support a Clone operation, then the GraphicTool can clone any kind of Graphic.

So in our music editor, each tool for creating a music object is an instance of
GraphicTool that's initialized with a different prototype. Each GraphicTool instance will produce a music object by
cloning its prototype and adding the clone
to the score.

We can use the Prototype pattern to reduce the number of classes even further.
We have separate classes for whole notes and half notes, but that's probably
unnecessary. Instead they could be instances of the same class initialized with
different bitmaps and durations. A tool for creating whole notes becomes just a
GraphicTool whose prototype is a MusicalNote initialized tobe a whole note. This
can reduce the number of classes in the system dramatically.It also makesit easier
to add a new kind of note to the music editor

---

Use the Prototype pattern when your code shouldn’t depend on the concrete classes of objects that you need to copy.

This happens a lot when your code works with objects passed to you from 3rd-party code via some interface. The concrete
classes of these objects are unknown, and you couldn’t depend on them even if you wanted to.

The Prototype pattern provides the client code with a general interface for working with all objects that support
cloning. This interface makes the client code independent from the concrete classes of objects that it clones.

Use the pattern when you want to reduce the number of subclasses that only differ in the way they initialize their
respective objects.

Suppose you have a complex class that requires a laborious configuration before it can be used. There are several common
ways to configure this class, and this code is scattered through your app. To reduce the duplication, you create several
subclasses and put every common configuration code into their constructors. You solved the duplication problem, but now
you have lots of dummy subclasses.

The Prototype pattern lets you use a set of pre-built objects configured in various ways as prototypes. Instead of
instantiating a subclass that matches some configuration, the client can simply look for an appropriate prototype and
clone it

### Known Uses

- ...

UI Frameworks: Creating new UI elements based on existing ones with slightly different configurations.
Document Generation: Creating copies of templates with different content or formatting.
Database Entity Creation: Instantiating similar database entities with varying attribute values.
Game Development: Generating new game objects/enemies based on prototypes with different behaviors or abilities.
Configuration Management: Generating different configurations based on a base configuration object.

GUI Applications: In graphical user interface development, creating similar windows or dialogs with slight variations in
layout or content. For instance, creating multiple windows with similar structures but different data fields or buttons.

E-commerce Platforms: Generating product objects with different specifications (size, color, price) based on a prototype
product. This allows for the creation of new products without recreating the entire object structure.

CAD Software: Creating copies of geometric shapes or complex objects with different properties or dimensions. For
instance, duplicating a 3D model with specific modifications.

Configuration Systems: In systems that handle configurations, such as setting up servers or network devices, the
Prototype pattern can be used to create different instances of configurations based on a base template.

Document Management Systems: Generating document objects with various structures, headers, footers, or formatting
options based on predefined templates.

Virtual Machines and Containers: Creating new instances of virtual machines or containers by cloning existing ones,
allowing for quicker deployment and resource optimization.

Simulation and Modeling: In scientific simulations or modeling systems, prototypes can be used to create variations of
initial models with altered parameters.

Educational Software: Generating exercises or quizzes with similar structures but different content or questions,
allowing for easy creation of diverse educational content.

Prototyping in Product Development: In hardware or industrial design, creating prototypes of products with slight
modifications to test variations without starting from scratch.

Messaging Applications: Creating message templates with different placeholders or formats for various communication
purposes (emails, notifications, etc.).

java.lang.Object#clone() (the class has to implement java.lang.Cloneable)

Perhaps the first example of the Prototype pattern was in Ivan Sutherland's Sketch-
pad system [Sut63]. The first widely known application of the pattern in an object-
oriented language was in ThingLab, where users could form a composite object
and then promote it to a prototype by installing it in a library of reusable ob-
jects [Bor81]. Goldberg and Robson mention prototypes as a pattern [GR831, but
Coplien [Cop92] gives a much more complete description. He describes idioms
related to the Prototype pattern for C++ and gives many examples and variations.
Etgdb is a debugger front-end based on ET++ that provides a point-and-click
interface to different line-oriented debuggers. Each debugger has a correspond-
ing Debugger Adaptor subclass. For example, Gdb Adaptor adapts etgdb to the command syntax of GNU gdb, while
SunDbxAdaptor adapts etgdb to Sun's dbx
debugger. Etgdb does not have a set of Debugger Adaptor classes hard-coded into
it. Instead, it reads the name of the adaptor to use from an environment vari-
able, looks for a prototype with the specified name in a global table, and then
clones the prototype. New debuggers can be added to etgdb by linking it with the
DebuggerAdaptor that works for that debugger.
The "interaction technique library" in Mode Composer stores prototypes of objects
that support various interaction techniques [Sha90]. Any interaction technique
created by the Mode Composer can be used as a prototype by placing it in this
library. The Prototype pattern lets Mode Composer support an unlimited set of
interaction techniques.
The music editor example discussed earlier is based on the Unidraw drawing
framework [VL90].

### Categorization

Purpose:  **Creational**  
Scope:    **Object**   
Mechanisms: **Polymorphism**

Creational design patterns abstract the instantiation process.
They help make a system independent of how its objects are created, composed, and represented.
A class creational pattern uses inheritance to vary the class that's instantiated,
whereas an object creational pattern will delegate instantiation to another object.

There are two recurring themes in these patterns.
First, they all encapsulate knowledge about which concrete classes the system uses.
Second, they hide how instances of these classes are created and put together.
All the system at large knows about the objects is their interfaces as defined by abstract classes.
Consequently, the creational patterns give you a lot of flexibility in what gets created, who creates it, how it gets
created, and when.

### Aspects that can vary

- Class of object that is instantiated.

### Solution to causes of redesign

- Creating an object by specifying a class explicitly.
    - Instantiating concrete classes directly in client code makes it harder to reuse and change.

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

Prototype has many of the same consequences that Abstract Factory (87) and
Builder (97) have: It hides the concrete product classes from the client, thereby
reducing the number of names clients know about. Moreover, these patterns let a
client work with application-specific classes withoutmodification.
Additional benefits of the Prototype pattern are listed below.

1. Adding and removing products at run-time. Prototypes let you incorporate a
   new concrete product class into a system simply by registering a prototypical instance with the client. That's a bit
   more flexible than other creational
   patterns, because a client can install and remove prototypes at run-time.
2. Specifying new objects by varying values. Highly dynamic systems let you define new behavior through object
   composition—by specifying values for a object's variables, for example—and not by defining new classes. You
   effectively define new kinds of objects by instantiating existing classes and
   registering the instances as prototypes of client objects. A client can exhibit
   new behavior by delegating responsibility to the prototype.
   This kind of design lets users define new "classes" without programming.
   In fact, cloning a prototype is similar to instantiating a class. The Prototype
   pattern can greatlyreduce the number of classes a systemneeds. In our music
   editor, one GraphicTool class can create a limitless variety of music objects.
3. Specifying new objects by varying structure. Many applications build objects
   from parts and subparts. Editors for circuit design, for example, build circuits out of subcircuits.1 For
   convenience, such applications often let you
   instantiate complex, user-defined structures, say,to use a specific subcircuit
   again and again.
   The Prototype pattern supports this as well. Wesimply add this subcircuit as
   a prototype to the palette of available circuit elements. As long as the composite circuit object implements Clone
   as a deep copy, circuits with different
   structures can be prototypes.
4. Reduced subclassing. FactoryMethod (107)often produces a hierarchy of Creator classes that parallels the product
   class hierarchy. The Prototype pattern
   lets you clone a prototype instead of asking a factory method to make a new
   object. Hence you don't need a Creator class hierarchy at all. This benefit
   applies primarily to languages like C++ that don't treat classes asfirst-class
   objects. Languages that do, like Smalltalk and ObjectiveC, derive less benefit, since you can always use a class
   object as a creator. Class objects already
   act like prototypes in these languages.
5. Configuring an application with classes dynamically. Some run-time environments let you load classes into an
   application dynamically. The Prototype
   pattern is the key to exploiting such facilities in a language likeC++.
   An application that wants to create instances of a dynamically loaded class
   won'tbe able to reference its constructor statically. Instead,the run-time environment creates an instance of each
   class automaticallywhen it's loaded, and
   it registers the instance with a prototype manager (see the Implementation
   section). Then the application can ask the prototype manager for instancesof
   newly loaded classes, classes that weren't linked with the program originally.
   The ET++ application framework [WGM88] has a run-time system that uses
   this scheme

The main liability of the Prototype pattern isthat each subclass of Prototype must
implement the Clone operation, which may be difficult. For example, adding
Clone is difficult when the classes under consideration already exist. Implementing Clone can be difficult when their
internals include objects that don't support
copying or have circular references.

You can clone objects without coupling to their concrete classes.
You can get rid of repeated initialization code in favor of cloning pre-built prototypes.
You can produce complex objects more conveniently.
You get an alternative to inheritance when dealing with configuration presets for complex objects.

Cloning complex objects that have circular references might be very tricky.

### Relations with Other Patterns

_Distinction from other patterns:_
_Combination with other patterns:_

- ...

Prototype and Abstract Factory (87)are competing patterns in some ways, as we
discuss at the end of this chapter. They can also be used together, however. An
Abstract Factory might store a set of prototypes from which to clone and return
product objects.
Designs that makeheavy use ofthe Composite (163)and Decorator (175)patterns
often can benefit from Prototype as well.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **A prototype class with a copy or clone method.**

### Structure

```mermaid
classDiagram
    class Prototype {
        <<interface>>
        + clone(): Prototype
    }

    class ConcretePrototype {
        + clone(): Prototype
    }

    Prototype <|.. ConcretePrototype: implements
```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

### Implementation

In the example we apply the prototype pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Prototype](https://refactoring.guru/design-patterns/prototype)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [Dotnet Core Central: Prototype Design Pattern (An Introduction for .NET Developers](https://youtu.be/OFwKNjnQJJ0?si=rwLCvywednjNKxaI)
- [Geekific: The Prototype Pattern Explained and Implemented in Java ](https://youtu.be/DcFhITC9v0E?si=BgjOSfbgV13kr-LF)

<br>
<br>
